import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

import { NgxMasonryOptions } from 'ngx-masonry';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  images = [];
  public masonryOptions: NgxMasonryOptions = {
		transitionDuration: '0.2s',
		gutter: 0,
		resize: true,
		initLayout: true,
    fitWidth: true,
    percentPosition: true,
	};

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.getUnsplashImages();
  }

  getUnsplashImages() {
    this.appService.getUnsplashImages()
      .subscribe((res:any)=> {
        this.images = [...res];
      })
  }

}
