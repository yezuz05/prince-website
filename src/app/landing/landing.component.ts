import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { CookiesService } from '@ngx-utils/cookies/src/cookies.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(private appService: AppService,
              private router: Router,
              private cookies: CookiesService,
              private meta: Meta) { }

  ngOnInit() {
    this.meta.updateTag({
        'description': `Welcome to Prince Akachi's Website. Select your preferred theme`
    });
  }

  goToHome(theme: string) {
    this.appService.setTheme(theme);
    this.cookies.put('theme', theme);
    this.router.navigate(['/home']);
  }

}
