import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { trigger, transition, style, animate } from '@angular/animations';
import { AppService } from '../app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('menuAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.2s', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('.3s', style({ opacity: 0 }))
      ])
    ])
  ],
})
export class HeaderComponent implements OnInit, OnDestroy {

  menuVisible = false;
  theme: string;
  routes = {
    1: '/home',
    2: '/portfolio',
    3: '/about',
    4: '/contact',
    5: 'https://www.uplabs.com/nuel_p',
    6: 'https://www.behance.net/user/?username=princenuelworks'
  };
  listenerFn: () => void;

  constructor(private router: Router,
              private renderer: Renderer2,
              private appService: AppService) {
                this.theme = this.appService.theme;
              }

  ngOnInit() {
    const htmlSelector = document.querySelector('html');
    htmlSelector.classList.remove('show-menu');
  }

  ngOnDestroy() {
    if (this.listenerFn) {
      this.listenerFn();
    }
  }

  toggleMenu() {
    this.menuVisible = !this.menuVisible;
    const htmlSelector = document.querySelector('html');
    if (this.menuVisible) {
      setTimeout(() => {
        htmlSelector.classList.add('show-menu');
      }, 600);
      this.listenerFn = this.renderer.listen(document, 'keyup', (event) => {
        if (+event.key >= 1) {
          if (+event.key >= 5) {
            window.open(this.routes[event.key], '_blank');
            return;
          }
          this.router.navigate([this.routes[event.key]]);
        }
      })
    } else {
        setTimeout(() => {
          htmlSelector.classList.remove('show-menu');
        }, 600);
    }
  }

}
