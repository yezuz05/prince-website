import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import Behance from 'behance/src';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  unsplash =  {
    baseUrl: 'https://api.unsplash.com',
    headers: {
      'accept-version': 'v1',
      'authorization': 'Client-ID 21db1657cd9858c335fc901b2e261311a2d13cc36da9e418dab45e5d333260da'
    }
  }
  theme: string;
  behance_key = '7PcLRXNrcuSWtrw2WHjHLeS1vI8qT0jM';
  behance = new Behance({ apiKey: this.behance_key, baseEndpoint: 'https://api.behance.net/v2' });

  constructor(private http: HttpClient) { }

  getUnsplashImages() {
    return this.http.get(`${this.unsplash.baseUrl}/users/princearkman/photos`,
     {headers: this.unsplash.headers, params: {order_by: 'popular'}}
    );
  }

  getBehanceProjects() {
    return this.behance.userProjects('princenuelworks', {sort: 'views'});
  }

  setTheme(theme: string) {
    this.theme = theme;
  }

}
