import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { AppService } from '../app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  behanceProject;
  theme;
  constructor(private title: Title,
              private meta: Meta,
              private appService: AppService) {
                this.theme = this.appService.theme;
              }

  ngOnInit() {
    this.title.setTitle(`Prince Akachi's Home Page`);
    this.meta.updateTag({
        'description': `Welcome to Prince Akachi's Home Page`
    });
    this.getBehanceProjects();
  }

  goTo(url:string) {
    window.open(url, '_blank');
  }

  getBehanceProjects() {
    this.appService.getBehanceProjects()
      .then((res) => {
        this.behanceProject = res.projects.sort((a, b) => b.stats.views - a.stats.views)[0];
      });
  }

}
