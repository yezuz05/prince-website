import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CookiesService } from '@ngx-utils/cookies/src/cookies.service';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'prince-website';

  constructor(private router: Router,
              private appService: AppService,
              private cookies: CookiesService) {

  }

  ngOnInit() {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          const scrollToTop = window.setInterval(() => {
            let pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 100); // how far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 16);
        }
      });
      const cookie_theme = this.cookies.get('theme');
      if (cookie_theme) {
        this.appService.setTheme(cookie_theme);
      }
  }
}
